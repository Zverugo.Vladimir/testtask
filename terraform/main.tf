###############################################################################
# Provider 
###############################################################################
provider "aws" {
  region = "us-east-1"
}

###############################################################################
# the IAM role for Lambda Function
###############################################################################
resource "aws_iam_role" "lambda_exec" {
  name = parse

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

###############################################################################
# Lambda Function
###############################################################################
resource "aws_lambda_function" "a-simple-app" {
  function_name = parse

  s3_bucket = "arn:aws:s3:::test-bucket-zv"
  s3_key    = "bucket_key_zv"
  handler   = "parse.task"
  runtime   = "python3.8"

  role = aws_iam_role.lambda_exec.arn
}

###############################################################################
# API Gateway 
###############################################################################
resource "aws_api_gateway_rest_api" "hello" {
  name        = "parse"
  description = "Serverless Application Example"
}

resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.hello.id
  parent_id   = aws_api_gateway_rest_api.hello.root_resource_id
  path_part   = "{proxy+}"
}
# The special path_part value "{proxy+}" activates proxy behavior, 
# which means that this resource will match any request path. 
# Similarly, the aws_api_gateway_method block uses a http_method of "ANY", 
# which allows any request method to be used. Taken together, 
# this means that all incoming requests will match this resource.

resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = aws_api_gateway_rest_api.hello.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.hello.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.a-simple-app.invoke_arn
}

resource "aws_api_gateway_method" "proxy_root" {
  rest_api_id   = aws_api_gateway_rest_api.hello.id
  resource_id   = aws_api_gateway_rest_api.hello.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambda_root" {
  rest_api_id = aws_api_gateway_rest_api.hello.id
  resource_id = aws_api_gateway_method.proxy_root.resource_id
  http_method = aws_api_gateway_method.proxy_root.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.a-simple-app.invoke_arn
}

resource "aws_api_gateway_deployment" "hello" {
  depends_on = [
    aws_api_gateway_integration.lambda,
    aws_api_gateway_integration.lambda_root,
  ]

  rest_api_id = aws_api_gateway_rest_api.hello.id
  stage_name  = "api"
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.a-simple-app.function_name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.hello.execution_arn}/*/*"
}

###############################################################################
# Backend 
###############################################################################

terraform {
  backend "s3" {
    bucket = "arn:aws:s3:::test-bucket-zv"
    key    = "bucket_key_zv"
    region = "us-east-1"
  }
}

#ata "terraform_remote_state" "state" {
# backend = "s3"
# config = {
#   bucket               = "tf-states-for-serverless"
#   key                  = "states/terraform.tfstate"
#   region               = "eu-west-1"
# }
#
