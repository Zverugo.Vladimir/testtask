variable "region" {
  default = "eu-east-1"
  type    = string
}

variable "function_name" {
  type        = string
  description = "Function name"
  default     = "a-funky-function"
}

# "node" is the filename within the zip file (node.js) and "handler"
# is the name of the property under which the handler function was
# exported in that file.
variable "handler" {
  type        = string
  description = "The handler for the serverless invocation"
  default     = "the-app/task.handler"
}

variable "runtime" {
  type        = string
  description = "The handler runtime for serverless invocation"
  default     = "python3.8"
}

variable "s3_bucket" {
  type        = string
  description = "The S3 bucket that holds the packaged function"
  default     = "test-bucket-zv"
}

variable "s3_key" {
}
